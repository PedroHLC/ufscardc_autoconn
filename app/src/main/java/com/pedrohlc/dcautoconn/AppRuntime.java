package com.pedrohlc.dcautoconn;

import android.app.Application;
import android.content.Context;
import android.content.IntentFilter;

/**
 * Created by pedrohlc on 4/3/17.
 */

public class AppRuntime extends Application {
    private static Application me = null;

    public void onCreate() {
        super.onCreate();
        me = this;

        final IntentFilter wRIntent = new IntentFilter();
        wRIntent.addAction(android.net.ConnectivityManager.CONNECTIVITY_ACTION);
        me.registerReceiver(new WifiReceiver(), wRIntent);
    }

    public static Context getLastRunContext() {
        return me.getApplicationContext();
    }
}
