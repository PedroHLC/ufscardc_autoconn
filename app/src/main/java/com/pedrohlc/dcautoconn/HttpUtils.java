package com.pedrohlc.dcautoconn;

import java.io.IOException;
import java.io.Reader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.util.Map;

import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

/**
 * Created by pedrohlc on 17/11/16.
 */

public class HttpUtils {
    public static final String ENCODING = "UTF-8";
    private static final TrustManager[] trustAllCerts = new TrustManager[]{
            new X509TrustManager() {
                public java.security.cert.X509Certificate[] getAcceptedIssuers() {
                    return null;
                }
                public void checkClientTrusted(
                        java.security.cert.X509Certificate[] certs, String authType) {
                }
                public void checkServerTrusted(
                        java.security.cert.X509Certificate[] certs, String authType) {
                }
            }
    };
    private static SSLSocketFactory sc_e = null;

    public static byte[] createPostData(Map<String,Object> params) throws UnsupportedEncodingException {
        StringBuilder postData = new StringBuilder();
        for (Map.Entry<String,Object> param : params.entrySet()) {
            if (postData.length() != 0) postData.append('&');
            postData.append(URLEncoder.encode(param.getKey(), ENCODING));
            postData.append('=');
            postData.append(URLEncoder.encode(String.valueOf(param.getValue()), ENCODING));
        }
        return postData.toString().getBytes(ENCODING);
    }

    public static String readAll(Reader rd) throws IOException {
        StringBuilder sb = new StringBuilder();
        int cp;
        while ((cp = rd.read()) != -1) {
            sb.append((char) cp);
        }
        return sb.toString();
    }

    public static void fuckSSL() {
        try {
            if(sc_e == null) {
                SSLContext sc = SSLContext.getInstance("SSL");
                sc.init(null, trustAllCerts, new java.security.SecureRandom());
                sc_e = sc.getSocketFactory();
            }
            HttpsURLConnection.setDefaultSSLSocketFactory(sc_e);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static URLConnection injectPost(URLConnection conn, Map<String,Object> params) throws IOException {
        byte[] postDataBytes = createPostData(params);

        ((HttpURLConnection)conn).setRequestMethod("POST");
        conn.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
        conn.setRequestProperty("Content-Length", String.valueOf(postDataBytes.length));
        conn.setDoOutput(true);
        conn.getOutputStream().write(postDataBytes);

        return conn;
    }
}
