package com.pedrohlc.dcautoconn;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.StrictMode;
import android.util.Log;
import android.widget.Toast;

import java.io.IOException;
import java.net.InetAddress;

/**
 * Created by pedrohlc on 17/11/16.
 */

@SuppressWarnings("MissingPermission")
public class WifiReceiver extends BroadcastReceiver {
    protected static final String LOG_PRE = "AutoDCConn";

    @Override
    public void onReceive(Context context, Intent intent) {
        doConn(context);
    }

    public static void doConn(Context context) {
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);

        ConnectivityManager conMan = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = conMan.getActiveNetworkInfo();
        if (netInfo != null && netInfo.getType() == ConnectivityManager.TYPE_WIFI) {
            WifiManager wifiManager = (WifiManager) context.getSystemService (Context.WIFI_SERVICE);
            WifiInfo info = wifiManager.getConnectionInfo ();
            String ssid  = info.getSSID();
            if((ssid.compareTo("\"DC-Grad\"") != 0)
                    && (ssid.compareTo("\"DC-Pos\"") != 0)
                    && (ssid.compareTo("\"DC-Prof\"") != 0))
                return;
            Log.d(LOG_PRE, "GLeISE");

            try {
                if(InetAddress.getByName("estadao.com.br").isReachable(3)) {
                    Log.d(LOG_PRE, "The cake is a lie!");
                    return;
                }
            } catch (IOException e) {
                e.printStackTrace();
                return;
            }

            AccountManager accMngr = AccountManager.get(context);
            Account[] accs = accMngr.getAccountsByType(Acc.ACC_TYPE);
            if(accs.length < 1) {
                Intent newAct = new Intent(context, Acc.class);
                newAct.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                context.startActivity(newAct);
                return;
            }

            Account mainAcc = accs[0];
            try {
                conMan.setNetworkPreference(ConnectivityManager.TYPE_WIFI);
                boolean result = Acc.doLogin(mainAcc.name, accMngr.getPassword(mainAcc));
                conMan.setNetworkPreference(ConnectivityManager.DEFAULT_NETWORK_PREFERENCE);
                if(!result)
                    Log.d(LOG_PRE, "Not enough mana!");
            } catch (IOException e) {
                e.printStackTrace();
            }
            Log.d(LOG_PRE, "They're watching us, they're all watching us!");

            Toast.makeText(context.getApplicationContext(), "Autenticado à rede!", Toast.LENGTH_LONG).show();
        }
    }
}
