package com.pedrohlc.dcautoconn;

import android.content.Context;
import android.content.res.Resources;

import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.io.InputStream;
import java.security.KeyStore;
import java.security.cert.Certificate;
import java.security.cert.CertificateFactory;

import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManagerFactory;

/**
 * Created by pedrohlc on 4/3/17.
 */

public class CpCompUfscarBrCert {
    private static boolean alreadyLoaded = false;
    private static SSLSocketFactory factory = null;

    public static boolean load() {
        try {
            CertificateFactory cf = CertificateFactory.getInstance("X.509");

            InputStream caInput = AppRuntime.getLastRunContext().getResources().openRawResource(R.raw.cpcompufscarbr);
            Certificate ca = cf.generateCertificate(caInput);
            caInput.close();

            String keyStoreType = KeyStore.getDefaultType();
            KeyStore keyStore = KeyStore.getInstance(keyStoreType);
            keyStore.load(null, null);
            keyStore.setCertificateEntry("ca", ca);

            String tmfAlgorithm = TrustManagerFactory.getDefaultAlgorithm();
            TrustManagerFactory tmf = TrustManagerFactory.getInstance(tmfAlgorithm);
            tmf.init(keyStore);

            SSLContext context = SSLContext.getInstance("TLS");
            context.init(null, tmf.getTrustManagers(), null);

            factory = context.getSocketFactory();
            return alreadyLoaded = true;
        } catch (Exception e) {
            e.printStackTrace();

            factory = null;
            return alreadyLoaded = false;
        }
    }

    public static SSLSocketFactory getSSLSocketFactory() {
        if(alreadyLoaded)
            return factory;
        else
            if(load())
                return factory;
            else
                return  HttpsURLConnection.getDefaultSSLSocketFactory();
    }
}
