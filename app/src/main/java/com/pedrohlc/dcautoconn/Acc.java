package com.pedrohlc.dcautoconn;

import android.accounts.Account;
import android.accounts.AccountAuthenticatorActivity;
import android.accounts.AccountManager;
import android.content.Intent;
import android.os.Bundle;
import android.os.StrictMode;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.nio.charset.Charset;
import java.util.LinkedHashMap;
import java.util.Map;

import javax.net.ssl.HttpsURLConnection;

public class Acc extends AccountAuthenticatorActivity {
    public static final String ACC_TYPE = "br.ufscar.dc_le";
    public static final String AUTH_URL = "https://cp.comp.ufscar.br:8003/index.php?zone=dc_le";
    private AccountManager accMngr;
    private EditText inID, inPsw;
    private Button btnDo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);

        super.onCreate(savedInstanceState);

        accMngr = AccountManager.get(this);
        setContentView(R.layout.activity_acc);
        inID = (EditText) findViewById(R.id.inID);
        inPsw = (EditText) findViewById(R.id.inPsw);
        btnDo = (Button) findViewById(R.id.btnDo);

        btnDo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String id = inID.getText().toString();
                String psw = inPsw.getText().toString();
                /*try {
                    if(doLogin(id, psw)) {*/
                        addAccount(id, psw);
                        beautifulFinish(id, psw);
                    /*}
                } catch (IOException e) {
                    e.printStackTrace();
                }*/
            }
        });
    }

    protected void beautifulFinish(String id, String psw) {
        final Intent intent = new Intent();
        intent.putExtra(AccountManager.KEY_ACCOUNT_NAME, id);
        intent.putExtra(AccountManager.KEY_ACCOUNT_TYPE, ACC_TYPE);
        intent.putExtra(AccountManager.KEY_AUTHTOKEN, psw);
        setAccountAuthenticatorResult(intent.getExtras());
        setResult(RESULT_OK, intent);
        finish();
    }

    public static boolean doLogin(String id, String psw) throws IOException {
        Map<String,Object> params = new LinkedHashMap<>();
        params.put("auth_user", id);
        params.put("auth_pass", psw);
        params.put("redirurl", "captiveportal-success.php");
        params.put("accept", "Concordo");

        URLConnection urlConnection = (new URL(AUTH_URL)).openConnection();
        ((HttpsURLConnection)urlConnection).setSSLSocketFactory(CpCompUfscarBrCert.getSSLSocketFactory());
        urlConnection = HttpUtils.injectPost(urlConnection, params);
        InputStream is = urlConnection.getInputStream();
        try {
            BufferedReader rd = new BufferedReader(new InputStreamReader(is, Charset.forName(HttpUtils.ENCODING)));
            return(HttpUtils.readAll(rd).contains("<h1>Sucesso!</h1>"));
        } finally {
            is.close();
            ((HttpURLConnection) urlConnection).disconnect();
        }
    }

    public void addAccount(String id, String psw) {
        final Account account = new Account(id, ACC_TYPE);
        accMngr.addAccountExplicitly(account, psw, null);
    }
}
