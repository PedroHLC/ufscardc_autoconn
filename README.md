# DC Auto Conn #

Auto authentication on Federal University of São Carlos's Department of Computing wireless networks.

Compatibles with: "DC-Grad", "DC-Pos" and "DC-Prof".

Latest review: April 3th, 2017.

[Download APK](https://www.dropbox.com/sh/9dghq6787hvbcca/AACPUrhCyXBVOiXBdFoNjMNta/ufscar_dc_autoconn.apk?dl=1)

[Google Play Store Page](https://play.google.com/store/apps/details?id=com.pedrohlc.dcautoconn)